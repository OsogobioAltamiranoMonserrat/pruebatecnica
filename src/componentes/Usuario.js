import React, { Component } from 'react';

class Usuario extends Component {

  render() {
    return (
      <div className = "card tarjetaUsuario">	
      	<div className = "card-body">
	      	<p><img src={this.props.datos.picture.thumbnail} className = "rounded"/></p>
	    
	      	<h3 className="card-title"><label>{this.props.datos.name.title}</label>&nbsp;
	      		  <label>{this.props.datos.name.first}</label>&nbsp;
	      		  <label>{this.props.datos.name.last}</label> 
	      	</h3>
	      	
	    	<div className="datosUsuario">
		      <img src="https://img.icons8.com/small/26/000000/email.png"/><label>{this.props.datos.email}</label><br/>
		      <img src="https://img.icons8.com/cotton/26/000000/gender.png"/><label>{this.props.datos.gender}</label><br/>
		      <img src="https://img.icons8.com/color/26/000000/phone.png"/><label>{this.props.datos.phone}</label><br/>
		      <img src="https://img.icons8.com/ios-glyphs/26/000000/touchscreen-smartphone.png"/><label>{this.props.datos.cell}</label>
	      	</div>
      	</div>
      </div>
    );
  }
}

export default Usuario;
