import React, { Component } from 'react';
import Modal from 'react-modal';
import Usuario from './Usuario.js';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

Modal.setAppElement('#root');

class TablaUsuarios extends Component {
  constructor(){
    super();
    this.state = {datos : [],
                  usuarioSeleccionado : {},
                  modalIsOpen : false}
    this.verPerfil = this.verPerfil.bind(this);
    this.cerrarModal = this.cerrarModal.bind(this);
  }

  componentWillMount(){
  fetch("https://randomuser.me/api/?results=10",{
    method : "GET"
   })
    .then(response => response.json())
    .catch(error => console.error('Error:', error))
    .then(respuesta => this.setState({datos : respuesta.results}))
  }
 
 verPerfil(usuario){
 	console.log(usuario);
  this.setState({usuarioSeleccionado : usuario,modalIsOpen : true});
 }
 
 cerrarModal(){
  this.setState({modalIsOpen : false});
 }

 ocultarMostrar(){
  let checkBox = document.getElementsByName('checkbox');
  for(let indice = 0; indice < checkBox.length; indice ++){
    let elementos = document.getElementsByClassName(checkBox[indice].value);
    for(let i = 0; i < elementos.length; i ++)
      if(checkBox[indice].checked)
        elementos[i].hidden = true; 
      else 
        elementos[i].hidden = false; 
  }
 }
 render() {
 	if(this.state.datos.length >0){
 	 let that = this, numeroUsuario = -1;
    	return (
       <div>
        <div className="checkbox opciones">
          <h4>Ocultar en Tabla</h4><br/>
          <label><input type="checkbox" value="title" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/user-female-circle.png"/>&nbsp;Title</label><br/>
          <label><input type="checkbox" value="first" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/name.png"/>&nbsp;First</label><br/>
          <label><input type="checkbox" value="last" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/name.png"/>&nbsp;Last</label><br/>
          <label><input type="checkbox" value="gender" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/office/26/000000/gender.png"/>&nbsp;Gender</label><br/>
          <label><input type="checkbox" value="email" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/email.png"/>&nbsp;Email</label><br/>
          <label><input type="checkbox" value="phone" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/phone.png"/>&nbsp;Phone</label><br/>
          <label><input type="checkbox" value="cell" onChange ={this.ocultarMostrar} name = "checkbox"/><img src="https://img.icons8.com/ultraviolet/26/000000/touchscreen-smartphone.png"/>&nbsp;Cell</label><br/>
      </div>    
      <div className="tabla">
      	<table className = "table table-hover table-sm">
         	<thead>
         		<tr>
         			<th className = "title">Title</th>
         			<th className = "first">First</th>
         			<th className = "last">Last</th>
         			<th className = "gender">Gender</th>
          		<th className = "email">Email</th>
             	<th className = "phone">Phone</th>
             	<th className = "cell">Cell</th>
             	<th></th>
             		</tr>
             	</thead>
          	 	<tbody>
          			{this.state.datos.map (function (usuario) {
               			numeroUsuario++;
		                return <tr key ={numeroUsuario}>
		                <td key = {"title"+numeroUsuario} className = "title">{usuario.name.title}</td>
		                <td key = {"first"+numeroUsuario} className = "first">{usuario.name.first}</td>
		                <td key = {"last"+numeroUsuario} className = "last">{usuario.name.last}</td>
		                <td key = {"gender"+numeroUsuario} className = "gender">{usuario.gender}</td>
		                <td key = {"email"+numeroUsuario} className = "email">{usuario.email}</td>
		                <td key = {"phone"+numeroUsuario} className = "phone">{usuario.phone}</td>
		                <td key = {"cell"+numeroUsuario} className = "cell">{usuario.cell}</td>
		                <td key = {"button"+numeroUsuario}><button onClick = {
		                    	function (e){
		                			e.preventDefault()
		                			that.verPerfil(usuario)
		                		}
		                	}>Ver Perfil</button>
		                </td>
		               </tr>
	                 })
    	            }
               </tbody>
          	</table>
           </div>
          	<Modal
        	 isOpen={this.state.modalIsOpen}
        	 style={customStyles}>
        	 <div >
            <Usuario  datos = {this.state.usuarioSeleccionado}/><br/>
            <p style = {{textAlign : "center"}}>
              <button className = "btn btn-info btn-sm" onClick = {this.cerrarModal}>Cerrar</button>
            </p>
           </div>        
     	    </Modal>
          </div>
        );}
    else
     return (<div></div>);	
  }
}

export default TablaUsuarios;
