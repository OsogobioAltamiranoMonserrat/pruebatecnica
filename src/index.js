import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import TablaUsuarios from './componentes/TablaUsuarios.js';

const App = () => (
   	 <div>
   	 	<TablaUsuarios />
   	 </div>
);

ReactDOM.render(<App />, document.getElementById('root'));

